package ru.bvd.cloudapp.api;

import ru.bvd.cloudapp.api.dto.task.TaskDto;

public interface TaskService {

    TaskDto addTask();

    TaskDto getTaskByGuid(String guid);
}
