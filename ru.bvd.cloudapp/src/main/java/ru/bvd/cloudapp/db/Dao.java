package ru.bvd.cloudapp.db;

import java.util.Collection;
import java.util.Optional;

interface Dao <T> {

    Optional<T> get(int id);

    Collection<T> getAll();

    T save(T t);

    void update(T t);

    void delete(T t);
}
