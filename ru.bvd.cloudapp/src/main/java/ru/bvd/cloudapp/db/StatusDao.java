package ru.bvd.cloudapp.db;

import ru.bvd.cloudapp.db.entity.Status;
import ru.bvd.cloudapp.db.entity.Task;

public interface StatusDao extends Dao<Status> {
    Status findStatusByCode(Task.TaskStatus code);

    Status findStatusById(Long id);

}
