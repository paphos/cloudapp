package ru.bvd.cloudapp.db;

import ru.bvd.cloudapp.db.entity.Task;
import ru.bvd.cloudapp.db.exception.NoDataFoundException;
import ru.bvd.cloudapp.db.exception.TooManyRowsException;

import java.util.Optional;
import java.util.UUID;

public interface TaskDao extends Dao<Task> {
    Optional<Task> getForUpdateNextRunningTask();

    Task findTaskByUuid(UUID uuid) throws NoDataFoundException, TooManyRowsException;
}
