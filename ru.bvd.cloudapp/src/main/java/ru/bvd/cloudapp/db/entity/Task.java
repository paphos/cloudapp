package ru.bvd.cloudapp.db.entity;

import java.time.LocalDateTime;
import java.util.UUID;

public class Task {

    public enum TaskStatus {
        CREATED("created"), RUNNING("running"), FINISHED("finished");

        private final String code;

        TaskStatus(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    public static Task of(Task task) {
        return new Task(task.id, task.getUuid(), task.status, task.timestamp);
    }

    final private Long id;

    final private UUID uuid;

    final private Status status;

    final private LocalDateTime timestamp;

    public Task(Long id, UUID uuid, Status status, LocalDateTime timestamp) {
        this.id = id;
        this.uuid = uuid;
        this.status = status;
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Status getStatus() {
        return status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", status=" + status +
                ", timestamp=" + timestamp +
                '}';
    }

}
