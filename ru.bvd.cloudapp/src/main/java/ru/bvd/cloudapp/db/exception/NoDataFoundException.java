package ru.bvd.cloudapp.db.exception;

public class NoDataFoundException extends Exception {
    public NoDataFoundException() {

    }

    public NoDataFoundException(String message) {
        super(message);
    }

    public NoDataFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
