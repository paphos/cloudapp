package ru.bvd.cloudapp.db.exception;

public class TooManyRowsException extends Exception {
    public TooManyRowsException() {

    }

    public TooManyRowsException(String message) {
        super(message);
    }

    public TooManyRowsException(String message, Throwable cause) {
        super(message, cause);
    }
}
