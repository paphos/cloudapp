package ru.bvd.cloudapp.db.impl;

import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.bvd.cloudapp.db.StatusDao;
import ru.bvd.cloudapp.db.TaskDao;
import ru.bvd.cloudapp.db.entity.Status;
import ru.bvd.cloudapp.db.entity.Task;
import ru.bvd.cloudapp.db.exception.DaoException;
import ru.bvd.cloudapp.db.exception.NoDataFoundException;
import ru.bvd.cloudapp.db.exception.TooManyRowsException;

import java.sql.*;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class TaskDaoImpl implements TaskDao {
    private static final String INSERT_SQL = "INSERT INTO t_task (c_uuid, c_status, c_timestamp) VALUES (?, ?, ?)";
    private static final String UPDATE_SQL = "UPDATE t_task set c_status = ?, c_timestamp = ?";

    private static final int INTERVAL_TASK_PROCESSING_SECOND = -120;
    private static final String SELECT_UPDATE_SQL_H2 = "" +
            "SELECT id, c_uuid, c_status, c_timestamp " +
            "FROM t_task t where c_status=? AND c_timestamp <= DATEADD('SECOND', " +INTERVAL_TASK_PROCESSING_SECOND + ", NOW())" +
            "FOR UPDATE";

    private static final String SELECT_UPDATE_SQL_P = "" +
            "SELECT id, c_uuid, c_status, c_timestamp " +
            "FROM t_task t where c_status=? AND c_timestamp <= NOW() - INTERVAL '120 second' " +
            "FOR UPDATE";


    public static final String SELECT_BY_UUID = "" +
            "SELECT id, c_uuid, c_status, c_timestamp FROM t_task WHERE c_uuid=?";

    private final JdbcTemplate jdbcTemplate;

    private final StatusDao statusDao;

    private final Environment environment;

    public TaskDaoImpl(JdbcTemplate jdbcTemplate, StatusDao statusDao, Environment environment) {
        this.jdbcTemplate = jdbcTemplate;
        this.statusDao = statusDao;
        this.environment = environment;
    }

    @Override
    public Optional<Task> get(int id) {
        return Optional.empty();
    }

    @Override
    public Collection<Task> getAll() {
        return null;
    }

    @Override
    public Task save(Task task) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
                    ps.setObject(1, task.getUuid());
                    ps.setLong(2, task.getStatus().getId());
                    ps.setTimestamp(3, Timestamp.valueOf(task.getTimestamp()));

                    return ps;
                },
                keyHolder
        );

        if (keyHolder.getKeyList().isEmpty()) {
            throw new DaoException("Error insert to db the task record. After insert key couldn't be null");
        }

        Long newTaskId = Long.valueOf(keyHolder.getKeyList().get(0).get("id").toString());

        return new Task(newTaskId, task.getUuid(), task.getStatus(), task.getTimestamp());
    }

    @Override
    public void update(Task task) {
        jdbcTemplate.update(UPDATE_SQL, task.getStatus().getId(), Timestamp.valueOf(task.getTimestamp()));
    }

    @Override
    public void delete(Task task) {

    }


    @Override
    public Optional<Task> getForUpdateNextRunningTask() {
        String sql;
        if (environment.getActiveProfiles().length>0 && "cloud".equals(environment.getActiveProfiles()[0])) {
            sql = SELECT_UPDATE_SQL_P;
        } else {
            sql = SELECT_UPDATE_SQL_H2;
        }


        List<Task> tasks = jdbcTemplate.query(sql, new TaskRowMapper(statusDao), statusDao.findStatusByCode(Task.TaskStatus.RUNNING).getId());

        if (tasks.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(tasks.get(0));
    }

    @Override
    public Task findTaskByUuid(UUID uuid) throws NoDataFoundException, TooManyRowsException{
        List<Task> tasks = jdbcTemplate.query(SELECT_BY_UUID, new TaskRowMapper(statusDao), uuid);

        if (tasks.isEmpty()) {
            throw new NoDataFoundException("Can't find task by uuid = " + uuid);
        }
        if (tasks.size() > 1) {
            throw new TooManyRowsException(tasks.size() + " tasks found by uuid = " + uuid);
        }

        return tasks.get(0);
    }

    public static class TaskRowMapper implements RowMapper<Task> {
        private final StatusDao statusDao;

        public TaskRowMapper(StatusDao statusDao) {
            this.statusDao = statusDao;
        }

        @Override
        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
            Status status = statusDao.findStatusById( rs.getLong("c_status") );
            return new Task(
                    rs.getLong("id"),
                    (UUID) rs.getObject("c_uuid"),
                    status,
                    rs.getTimestamp("c_timestamp").toLocalDateTime()
            );
        }
    }
}
