package ru.bvd.cloudapp.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.bvd.cloudapp.api.TaskService;
import ru.bvd.cloudapp.api.dto.task.TaskDto;

import java.util.UUID;

@RestController

public class TaskRestController {

    private final TaskService taskService;

    public TaskRestController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/task")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public TaskDto addTask() {
        return taskService.addTask();
    }

    @GetMapping("/task/{guid}")
    public TaskDto getTask(@PathVariable("guid") String guid) {
        try {
            return taskService.getTaskByGuid(guid);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

    }

}
