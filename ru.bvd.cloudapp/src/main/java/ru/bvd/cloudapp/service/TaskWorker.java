package ru.bvd.cloudapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.bvd.cloudapp.db.StatusDao;
import ru.bvd.cloudapp.db.TaskDao;
import ru.bvd.cloudapp.db.entity.Task;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class TaskWorker {
    private final Logger log = LoggerFactory.getLogger( getClass() );

    private final TaskDao taskDao;
    private final StatusDao statusDao;

    public TaskWorker(TaskDao taskDao, StatusDao statusDao) {
        this.taskDao = taskDao;
        this.statusDao = statusDao;
    }

    @Scheduled(fixedDelay = 1000)
    public void process() {
        Optional<Task> oTask = taskDao.getForUpdateNextRunningTask();
        if (!oTask.isPresent()) {
            return;
        }

        Task task = oTask.get();

        taskDao.update(
                new Task(task.getId(), task.getUuid(), statusDao.findStatusByCode(Task.TaskStatus.FINISHED), LocalDateTime.now())
        );


        log.debug("worker processed task: " + task);
    }

}
