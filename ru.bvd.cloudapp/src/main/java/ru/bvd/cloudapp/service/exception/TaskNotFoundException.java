package ru.bvd.cloudapp.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Task Not Found")
public class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException() {

    }

    public TaskNotFoundException(String message) {
        super(message);
    }

    public TaskNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskNotFoundException (Throwable cause) {
        super(cause);
    }
}
